Rails.application.routes.draw do
  root 'pages#home'
  resources :jobs

  devise_for :users

 get '/help', to: 'pages#help', as: 'help_path'

 get '/applied_user/:job_id', to: 'jobs#applied_user', as: 'applied_user'
 delete '/cancel/:job_id', to: 'jobapplications#destroy', as:'cancel_path'
 
 post'/apply/:job_id', to: 'jobapplications#create', as: 'apply_path'
 get '/my_applied_jobs', to: 'jobapplications#my_applied_jobs', as: 'my_applied_jobs'
   # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
 end
