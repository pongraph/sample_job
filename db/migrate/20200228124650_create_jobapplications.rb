class CreateJobapplications < ActiveRecord::Migration[6.0]
  def change
    create_table :jobapplications do |t|
    	t.references :user, index: true, foreign_key: true
    	t.references :job, index: true, foreign_key: true
      t.timestamps
    end
  end
end
