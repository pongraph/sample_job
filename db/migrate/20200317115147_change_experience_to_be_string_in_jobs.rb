class ChangeExperienceToBeStringInJobs < ActiveRecord::Migration[6.0]
  def change
  	change_column :jobs, :experience, :string
  end
end
