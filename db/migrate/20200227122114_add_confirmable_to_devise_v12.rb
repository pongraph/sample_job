class AddConfirmableToDeviseV12 < ActiveRecord::Migration[6.0]
  def change
    change_table(:users) do |t|
      t.boolean :confirmable, :default => false
    end
  end
  end
