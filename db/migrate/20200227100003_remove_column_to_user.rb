class RemoveColumnToUser < ActiveRecord::Migration[6.0]
  def change
  	remove_column :jobs, :job_author, :string
  	remove_column :jobs, :apply_url, :string
  end
end
