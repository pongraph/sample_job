class JobsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_job, only: %i[show edit update destroy]

  # GET /jobs
  def index
    if current_user.roles == "employer"
      _jobs = Job.where(:user_id => current_user.id)
    else
      _jobs = Job.all.order("created_at desc")
    end
    @jobs = _jobs.page(params[:page]).per(10)
  end

  # GET /jobs/1
  def show
    authorize! :read, @job
  end

  # GET /jobs/new
  def new
    @job = current_user.jobs.new
  end

  # GET /jobs/1/edit
  def edit
    authorize! :edit, @job
  end

  # POST /jobs
  def create
    @job = current_user.jobs.build(job_params)
    authorize! :create, @job
    @job.user_id = current_user.id

    if @job.save
      redirect_to @job, notice: "job create successfully"
    else
      render :new
    end
  end

  # PATCH/PUT /jobs/1
  def update
    authorize! :update, @job

    if @job.update(job_params)
      redirect_to @job
      flash[:notice] = "job updated successfully !"
    else
      render :edit
    end
  end

  # DELETE /jobs/1
  def destroy
    authorize! :delete, @job
    @job.destroy
    
    redirect_to jobs_url, notice: 'Job was successfully destroyed.'
  end

  def applied_user
     @job = Job.find(params[:job_id])
  end 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
       @job = Job.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def job_params
      params.require(:job).permit(:title, :description, :salary, :experience)
    end
end
   