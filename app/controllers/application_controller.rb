class ApplicationController < ActionController::Base
	rescue_from CanCan::AccessDenied do |exception|

  	redirect_to root_url, alert: exception.message
  end
  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    jobs_path
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:roles])
  end
end
