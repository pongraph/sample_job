class JobapplicationsController < ApplicationController  
  def create
    @job =Job.find(params[:job_id])
    job_application = Jobapplication.new(job: @job, user: current_user, isapplied: true)

    authorize! :create, job_application

    if job_application.save
      UserMailer.welcome_email(@job).deliver_now
    end
  end     
      
  def destroy
    @job =Job.find(params[:job_id])
    job_application = Jobapplication.find_by(job_id: @job, user_id: current_user)

    authorize! :destroy, job_application

    if job_application.destroy
      flash[:notice] = "You cancel the job, good luck!"
    end
  end

   def my_applied_jobs
    authorize! :my_applied_jobs, Jobapplication
    @my_applied_jobs = Jobapplication.where(user: current_user)
   end 
end