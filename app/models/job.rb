class Job < ApplicationRecord
	belongs_to :user
	has_many :jobapplications, dependent: :destroy
  validates_presence_of :user_id, :title, :description, :experience, :salary
end
