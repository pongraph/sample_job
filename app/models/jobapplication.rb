class Jobapplication < ApplicationRecord
	belongs_to :user
	belongs_to :job
	validates :user, uniqueness: { scope: :job,
    message: "you already applied" }
end
