class UserMailer < ApplicationMailer
	default from: 'notifications@example.com'

 def welcome_email(job)
    	e_mail = job.user.email
    	mail(to: e_mail, subject: 'jobapplication')
 end
 end 
